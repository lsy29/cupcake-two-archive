import {
  Container, Col, Row, Jumbotron, Button, Card, CardImg, CardText, CardBody,
  CardTitle, CardSubtitle
} from 'reactstrap';
import { Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import { useDispatch, useSelector } from 'react-redux';
import {push} from 'connected-react-router'
import { useEffect, useState } from 'react';
import { IRootState } from '../redux/store';
//edgar
const {REACT_APP_API_SERVER} = process.env

export default function ExAndQuestion() {
  const dispatch = useDispatch();
  //testing testing testing testing testing testing testing testing testing testing testing testing testing testing 
  const selectedVideoId = useSelector((state:IRootState)=> state.video.selectedVideo)

  const [Exercise, setExercise] = useState<any>(null)
  useEffect(() => {
    //edgar
    fetch(`${REACT_APP_API_SERVER}/exercise?vid=`+selectedVideoId)
      .then(response => response.json())
      .then(data => setExercise(data)); 
  }, []);
  console.log("fetch exercise", Exercise)
  //testing

  return (
    <div>
        <div style={{ display: "flex", justifyContent: "center" }}>
          <Jumbotron style={{ width: "900px", height: "350px", margin: "20px", marginBottom: "40px", overflowX: "scroll" }}>
            <Container fluid>
              <p className="lead-2">
               {Exercise && Exercise.length>0 ?Exercise[0].content:'testing'}
            </p>
            </Container>
          </Jumbotron>
        </div>

      <Row style={{display:"flex", alignItems:"baseline",justifyContent:"space-between", marginBottom:"30px"}}>
      <FormGroup>
        <Input type="email" name="email" id="exampleEmail" placeholder="Q1" style={{textAlign:"center"}} />
      </FormGroup>
      <FormGroup>
        <Input type="email" name="email" id="exampleEmail" placeholder="Q2" style={{textAlign:"center"}}/>
      </FormGroup>
      <FormGroup>
        <Input type="email" name="email" id="exampleEmail" placeholder="Q3" style={{textAlign:"center"}}/>
      </FormGroup>
      <FormGroup>
        <Input type="email" name="email" id="exampleEmail" placeholder="Q4" style={{textAlign:"center"}}/>
      </FormGroup>
      <FormGroup>
        <Input type="email" name="email" id="exampleEmail" placeholder="Q5" style={{textAlign:"center"}}/>
      </FormGroup>
      <Button onClick={() => dispatch(push('/check-answer'))}>Check Answers from Comp</Button>
      </Row>

    </div>
  )
}

