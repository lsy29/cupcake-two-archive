import {
  Container, Col, Row, Jumbotron, Button, Card, CardImg, CardText, CardBody,
  CardTitle, CardSubtitle
} from 'reactstrap';
// import { useDispatch, useSelector } from 'react-redux';
// import {IRootState} from '../redux/store'



const UserAndVideo = (props: any) => {
  // const dispatch = useDispatch();
  // const userInfo = useSelector ((state: IRootState) => state.auth.user)

  
  return (
    <div>
      <Row>

        <Col>

          <Card style={{
            marginBottom: "30px", width: "300px", height: "315px", display: "flex",
            alignItems: "center",
            justifyContent: "center"
          }}>
            <CardImg style={{ width: "200px", height: "200px" }} src="/img/user-dummy.png" alt="Card image cap" />
            <CardBody>
              <CardTitle tag="h2" style={{ display: "flex", alignItems: "center", justifyContent: "center" }}>
                username </CardTitle>
              <CardText>Some quick example text to build on the card title and make up the bulk of the card's content.</CardText>
            </CardBody>
          </Card>

        </Col>

        <Col>
          <iframe width="560" height="315" src={"https://www.youtube.com/embed/iNb7FdG9ucw?controls=0"}
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
          ></iframe>
        </Col>

      </Row>


    </div>
  );
};

export default UserAndVideo;