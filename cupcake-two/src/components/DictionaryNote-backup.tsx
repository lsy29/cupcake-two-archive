import { Navigation } from '../layout/navigation'
import './DictionaryNote.css'
import { Contact } from '../layout/footer'
import {
    Card, Button, CardHeader, CardFooter, CardBody,
    CardTitle, CardText, Container
} from 'reactstrap';
import { ImBin } from 'react-icons/im';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { deleteVocabThunk, loadAllMyVocabThunk } from '../redux/myVocab/thunk';
import { isNotEmittedStatement } from 'typescript';
import { IRootState, IRootThunkDispatch } from '../redux/store';
import { AiTwotoneSound } from 'react-icons/ai';



export default function DictionaryNote() {

    const dispatch = useDispatch();

    // const [myVocabs, setMyVocabs] = useState<any>({})
    // const selectedDeleId = useSelector((state:IRootState)=> state.myVocab.vocabId)
    const words = useSelector((state: IRootState) => state.myVocab.words)

    console.log("word from state: ", words);


    useEffect(() => {
        // let async getResult = await loadAllMyVocabThunk()
        //             .then(response => response.json())
        //             .then(result => setMyVocabs(result))
        dispatch(loadAllMyVocabThunk())

    }, [])

    function onDelete(noteId: any) {
        dispatch(deleteVocabThunk(noteId))
    }

    return (
        <div>
            {/* {myVocabs? myVocabs.map((noteItem)=>{
<card id={isNotEmittedStatement.id} >
    <div></div>
    <button onClick={onDelete(noteItem.id)}></button>
</card>
}):""} */}

            <Navigation />
            <Container >
                <div className="notebook-container">
                    {words?.map((myVocabItems) => {
                        return (

                            <Card className="notebook-whole" key={myVocabItems.id} style={{ width: "25rem", marginTop: "5rem", display: "flex" }}>
                                <CardHeader className="notebook-header"> {myVocabItems.word}
                                    <ImBin className="deleteBTN" onClick={() => onDelete(myVocabItems.id)} />

                                </CardHeader>
                                <CardBody >
                                    <CardText>
                                        {/* phonetics layer */}
                                        <div>
                                            {myVocabItems.phonetics && myVocabItems.phonetics.length > 0 ? myVocabItems.phonetics!.map((phonetic: any) =>
                                                <div>
                                                    <div className="plus-circle" onClick={() => {
                                                        let audio = new Audio(phonetic.audio)
                                                        audio.play()
                                                    }}> {phonetic.text}
                                                        <AiTwotoneSound />
                                                    </div>

                                                    {/* <b>{phonetic.audio}</b> */}

                                                </div>

                                            )
                                                : ""
                                            }
                                        </div>


                                    </CardText>


                                    {/* meanings layer */}
                                    {myVocabItems.meanings && myVocabItems.meanings.length > 0 ? myVocabItems.meanings.map((meaning: any) => {
                                        let partOfSpeechSplit = meaning.partOfSpeech.split(/\s/).reduce((response: any, word: any) => response += word.slice(0, 1), '')
                                        let partOfSpeechUI = partOfSpeechSplit.split("").reverse().join('.');

                                        return (
                                            <p>
                                                <Card>
                                                    <CardText className="vocab-insideCard">
                                                        <div className="partOfSpeechUI">
                                                            {partOfSpeechUI}
                                                            {/* {console.log("meaning: ", meaning)} */}
                                                        </div>

                                                    </CardText>

                                                    <CardText>

                                                        {meaning.definitions && meaning.definitions.length > 0 ? meaning.definitions.map((definition: any) => {
                                                            return (
                                                                <div className="meaning-insideCard">
                                                                    <b><i>definition:</i></b>
                                                                    <p className="vocab-inner">{definition.definition}</p>
                                                                    {/* {console.log("definition: ", definition.definition)} */}


                                                                    {definition.synonyms && definition.synonyms.length > 0 ?
                                                                        // definition.synonyms.map((synonym: any) => {
                                                                        //     let firstSynonym = synonym.from(synonym)[0]

                                                                        // return (
                                                                        <div>
                                                                            <b><i>synonym:</i></b>
                                                                            <p className="vocab-inner">{" \" " + definition.synonyms.join(", \" ") + " \" "}</p>
                                                                            {/* {console.log("synonym: ", synonym)} */}
                                                                        </div>
                                                                        // )
                                                                        // })
                                                                        :
                                                                        ""}

                                                                    {definition.example && definition.example.length > 0 ?
                                                                        <div>
                                                                            <b><i>example:</i></b>
                                                                            <p className="vocab-inner">{definition.example}</p>
                                                                            {/* {console.log("synonym: ", synonym)} */}
                                                                        </div>
                                                                        :
                                                                        ""}

                                                                </div>

                                                            )



                                                        }) :
                                                            ""}


                                                    </CardText>
                                                </Card>
                                            </p>

                                        )

                                    }) :

                                        ""}

                                    {/* Hi */}

                                </CardBody>


                            </Card>
                        )
                    })}


                    {/* card template*/}
                    {/* <Card style={{ width: "25rem", marginTop:"5rem", display: "flex"}}>
                    <CardHeader>Header</CardHeader>
                    <CardBody>
                        <CardTitle tag="h5">Special Title Treatment</CardTitle>
                        <CardText>With supporting text below as a natural lead-in to additional content.</CardText>
                        <Button>Go somewhere</Button>
                    </CardBody>
                    <CardFooter>Footer</CardFooter>
                </Card> */}

                </div>
            </Container>
            <Contact />

        </div>
    )
}