import {
    Row, Col, Card, CardImg, CardText, CardBody,
    CardTitle, CardSubtitle, Button
} from 'reactstrap';
import { useDispatch } from 'react-redux';
import { push } from 'connected-react-router'
import React, { useEffect } from 'react';
import { selectCat } from '../redux/video/action';


export const ChannelTesting = (props: any) => {

    // useEffect(() => {
    //     const requestOptions = {
        
    //     fetch(‘https://reqres.in/api/posts', requestOptions)
    //     .then(response => response.json())
    //     .then(data => setPostId(data.id));
        
    //     // empty dependency array means this effect will only run once (like componentDidMount in classes)
    //     }, []);

    function onCatClick(catId:number){
        dispatch(selectCat(catId))
        dispatch(push('/videos'))
    }

    const dispatch = useDispatch();
    return (
        <div id='portfolio' className='text-center'>
            <div >
                <div className='section-title'>
                    <h2 style={{ fontSize: "3.5rem", fontWeight:'bold'}}>Channel</h2>
                    <p style={{ fontSize: "2.5rem"}}>
                        We offer various channels for you to learn English!
                    </p>
                </div>

                <div >
                    {/* <Row style={{ display: "flex", justifyContent: "center", flexWrap:"initial",}}> */}
                    <Row style={{ display: "flex", justifyContent: "center", flexWrap:"wrap"}}>

                        <div>
                        {/* <Card style={{ marginBottom: "60px" }}> */}
                            <Card style={{ margin: "2 5rem" }}>
                                <img
                                    src='img/portfolio/netflix-3.jpeg'
                                    style={{ width: "55rem", height: "35rem" }}
                                />
                                <CardBody>
                                    <CardText style={{ fontSize: "2rem"}}>Netflix</CardText>
                                    {/* <Button onClick={() => dispatch(push('/video-testing'))}>Click to see more</Button> */}
                                    <Button style={{fontSize:"1.5rem"}} onClick={() => onCatClick(1)}>Click to see more</Button>
                                </CardBody>
                            </Card>

                        </div>
                        <div>
                        <Card style={{ margin: "2 5rem", marginBottom:"6rem"}}>
                                <img
                                    src='img/portfolio/disney.jpg'
                                    style={{ width: "55rem", height: "35rem" }}
                                />
                                <CardBody>
                                    <CardText style={{ fontSize: "2rem"}}>Disney</CardText>
                                    <Button style={{fontSize:"1.5rem"}} onClick={() => onCatClick(2)}>Click to see more</Button>
                                </CardBody>
                            </Card>
                        </div>
                        <div>
                        <Card style={{ margin: "2 5rem", marginBottom:"6rem" }}>
                                <img
                                    src='img/portfolio/bbc_news.jpg'
                                    style={{ width: "55rem", height: "35rem" }}
                                />
                                <CardBody>
                                    <CardText style={{ fontSize: "2rem"}}>BBC News</CardText>
                                    {/* <Link to={'/video/cat/' + cat} key={cat}> */}
                                    <Button style={{fontSize:"1.5rem"}} onClick={() => onCatClick(0)}>Click to see more</Button>
                                    {/* </Link> */}
                                </CardBody>
                            </Card>
                        </div>
                    {/* </Row>
                    <Row style={{ display: "flex", justifyContent: "center", flexWrap:"initial" }}> */}
                        <div>
                        <Card style={{ margin: "2 5rem", marginBottom:"6rem" }}>                              <img
                                    src='img/portfolio/health.jpg'
                                    style={{ width: "55rem", height: "35rem" }}
                                />
                                <CardBody>
                                    <CardText style={{ fontSize: "2rem"}}>Health</CardText>
                                    <Button style={{fontSize:"1.5rem"}} onClick={() => onCatClick(3)}>Click to see more</Button>
                                </CardBody>
                            </Card>

                        </div>
                        <div>
                        <Card style={{ margin: "2 5rem", marginBottom:"6rem"}}>                               <img
                                    src='img/portfolio/science.jpeg'
                                    style={{ width: "55rem", height: "35rem" }}
                                />
                                <CardBody>
                                    <CardText style={{ fontSize: "2rem"}}>Science</CardText>
                                    <Button style={{fontSize:"1.5rem"}} onClick={() => onCatClick(5)}>Click to see more</Button>
                                </CardBody>
                            </Card>
                        </div>
                        <div>
                        <Card style={{ margin: "2 5rem", marginBottom:"6rem" }}>                              <img
                                    src='img/portfolio/travel.jpeg'
                                    style={{ width: "55rem", height: "35rem" }}
                                />
                                <CardBody>
                                    <CardText style={{ fontSize: "2rem"}}>Travel</CardText>
                                    <Button style={{fontSize:"1.5rem"}} onClick={() => onCatClick(4)}>Click to see more</Button>
                                </CardBody>
                            </Card>
                        </div>
                        
                    {/* </Row>
                    <Row style={{ display: "flex", justifyContent: "center", flexWrap:"initial" }}> */}

                    <div>
                    <Card style={{ margin: "2 5rem", marginBottom:"6rem" }}>                              <img
                                    src='img/portfolio/lifestyle.jpg'
                                    style={{ width: "55rem", height: "35rem" }}
                                />
                                <CardBody>
                                    <CardText style={{ fontSize: "2rem"}}>Lifestyle</CardText>
                                    <Button style={{fontSize:"1.5rem"}} onClick={() => onCatClick(6)}>Click to see more</Button>
                                </CardBody>
                            </Card>
                            </div>
                            <div>
                            <Card style={{ margin: "2 5rem", marginBottom:"6rem" }}>                              <img
                                    src='img/portfolio/gaming.jpg'
                                    style={{ width: "55rem", height: "35rem" }}
                                />
                                <CardBody>
                                    <CardText style={{ fontSize: "2rem"}}>Gaming</CardText>
                                    <Button style={{fontSize:"1.5rem"}} onClick={() => onCatClick(7)}>Click to see more</Button>
                                </CardBody>
                            </Card>
                        </div>
                        <div>
                        <Card style={{ margin: "2 5rem", marginBottom:"6rem" }}>                               <img
                                    src='img/portfolio/finance.jpg'
                                    style={{ width: "55rem", height: "35rem" }}
                                />
                                <CardBody>
                                    <CardText style={{ fontSize: "2rem"}}>Finance</CardText>
                                    <Button style={{fontSize:"1.5rem"}} onClick={() => onCatClick(8)}>Click to see more</Button>
                                </CardBody>
                            </Card>
                        </div>
                        </Row>
                </div>
            </div>
        </div>
    )
}
