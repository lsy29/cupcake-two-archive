
import {
  Card, CardImg, CardText, CardBody,
  CardTitle, CardSubtitle, Button, Container
} from 'reactstrap';
import '../components/QRcode.css'
import { BiLogOut } from 'react-icons/bi';


import React, { useState, useRef, useEffect } from 'react';

// import {Container, Card, CardBody, makeStyles, Grid, TextField, Button} from 'reactstrap';
import QRCode from 'qrcode';
import QrReader from 'react-qr-reader';
import { useSelector } from 'react-redux';
import { IRootState } from '../redux/store';
// import { Navigation } from '../layout/navigation';
import Logo from '../layout/CUPCAKE.gif';









export default function WithQRcode() {
  const [tokenQR, setTokenQR] = useState('');
  const [scanResult, setScanResult] = useState('');
  const [scanResultFile, setScanResultFile] = useState('');
  const [scanResultWebCam, setScanResultWebCam] = useState('');
  const qrRef = useRef(null) as any;


  const token = useSelector(
    (state: IRootState) => state.auth.token
  )

  const generateQrCode = async () => {
    try {
      const response = await QRCode.toDataURL(tokenQR);
      setScanResult(response);
      console.log("scanResult: ", scanResult);
      return scanResult;
    } catch (error) {
      console.log(error);
    }
  }

  useEffect(() => {
    if (!token) {
      setTokenQR('');
    } else {
      setTokenQR(token);
      generateQrCode()

    }

  }, [])

  useEffect(() => {

    generateQrCode();


  }, [tokenQR])

  const handleErrorFile = (error: any) => {
    console.log(error);
  }
  // const handleScanFile = (result: any) => {
  //     if (result) {
  //         setScanResultFile(result);
  //     }
  // }

  const handleScanFile = (tokenQR: any) => {
    if (tokenQR) {
      setScanResultFile(tokenQR);
      return scanResultFile;
    }
  }

  useEffect(() => {
    handleScanFile(tokenQR);
  }, [tokenQR])


  const onScanFile = () => {
    qrRef!.current!.openImageDialog!();
  }

  const handleErrorWebCam = (error: any) => {
    console.log(error);
  }
  const handleScanWebCam = (result: any) => {
    if (result) {
      setScanResultWebCam(result);
    }
  }


  return (

    <div className="BGbox">

      <a href="/">
        <img className="cupcakeLogo" src={Logo} style={{ width: "70px", height: "70px" }} />
      </a>

      <Card className="box-container">


        <CardBody >
          {scanResult ? (
            <a href={scanResult} download>
              <img className="QRIMG" src={scanResult} alt="img" />
            </a>) : null}
        </CardBody>


        <CardBody className="box-main" >
          <CardTitle className="box-title">Scan to auto-login </CardTitle>

          <CardText className="box-step">Step 1: Open your scanner on your phone </CardText>
          <CardText className="box-step">Step 2: Scan the QR code </CardText>
          <CardText className="box-step">Step 3: Done</CardText>
          <a href="/">
            <BiLogOut className="box-icon">Back to Home</BiLogOut>
          </a>
          <CardText></CardText>
        </CardBody>
      </Card>




    </div>
  );
}









