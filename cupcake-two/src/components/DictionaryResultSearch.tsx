import './DictionaryResult.css'
// import { Contact } from '../layout/footer'
import {
    Card, Button, CardHeader, CardFooter, CardBody,
    CardTitle, CardText, Container
} from 'reactstrap';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { deleteVocabThunk, loadAllMyVocabThunk } from '../redux/myVocab/thunk';
// import { isNotEmittedStatement } from 'typescript';
import { IRootState, IRootThunkDispatch } from '../redux/store';
import { AiTwotoneSound } from 'react-icons/ai';
import { searchVocabThunk } from '../redux/notebook/thunk';



export default function DictionaryResultSearch(){


    const dispatch = useDispatch();

    // const [myVocabs, setMyVocabs] = useState<any>({})
    // const selectedDeleId = useSelector((state:IRootState)=> state.myVocab.vocabId)
    const words = useSelector((state: IRootState) => state.myVocab.words)
    // const words = useSelector((state: IRootState) => state.notebook.word)

    console.log("word from state: ", words);

    function searchWord() {
        let searchWord: string = (document.querySelector('#searchWord') as any).value
        dispatch(searchVocabThunk(searchWord))
    }

    // useEffect(() => {
    //     // let async getResult = await loadAllMyVocabThunk()
    //     //             .then(response => response.json())
    //     //             .then(result => setMyVocabs(result))
    //     dispatch(loadAllMyVocabThunk())

    // }, [])

    return (
        <div>
            {words?.map((myVocabItems) => {
                return (
                    <div>
                        <CardBody >
                            <CardText>
                                {/* phonetics layer */}
                                <div>
                                    {myVocabItems.phonetics && myVocabItems.phonetics.length > 0 ? myVocabItems.phonetics!.map((phonetic: any) =>
                                        <div>
                                            <div className="plus-circle" onClick={() => {
                                                let audio = new Audio(phonetic.audio)
                                                audio.play()
                                            }}> {phonetic.text}
                                                <AiTwotoneSound />
                                            </div>

                                            {/* <b>{phonetic.audio}</b> */}

                                        </div>

                                    )
                                        : ""
                                    }
                                </div>


                            </CardText>


                            {/* meanings layer */}
                            {myVocabItems.meanings && myVocabItems.meanings.length > 0 ? myVocabItems.meanings.map((meaning: any) => {
                                let partOfSpeechSplit = meaning.partOfSpeech.split(/\s/).reduce((response: any, word: any) => response += word.slice(0, 1), '')
                                let partOfSpeechUI = partOfSpeechSplit.split("").reverse().join('.');

                                return (
                                    <div>
                                        <p>
                                            <Card>
                                                <CardText className="vocab-insideCard">
                                                    <div className="partOfSpeechUI">
                                                        {partOfSpeechUI}
                                                        {/* {console.log("meaning: ", meaning)} */}
                                                    </div>

                                                </CardText>

                                                <CardText>

                                                    {meaning.definitions && meaning.definitions.length > 0 ? meaning.definitions.map((definition: any) => {
                                                        return (
                                                            <div className="meaning-insideCard">
                                                                <b><i>definition:</i></b>
                                                                <p className="vocab-inner">{definition.definition}</p>
                                                                {/* {console.log("definition: ", definition.definition)} */}


                                                                {definition.synonyms && definition.synonyms.length > 0 ?
                                                                    // definition.synonyms.map((synonym: any) => {
                                                                    //     let firstSynonym = synonym.from(synonym)[0]

                                                                    // return (
                                                                    <div>
                                                                        <b><i>synonym:</i></b>
                                                                        <p className="vocab-inner">{" \" " + definition.synonyms.join(", \" ") + " \" "}</p>
                                                                        {/* {console.log("synonym: ", synonym)} */}
                                                                    </div>
                                                                    // )
                                                                    // })
                                                                    :
                                                                    ""}

                                                                {definition.example && definition.example.length > 0 ?
                                                                    <div>
                                                                        <b><i>example:</i></b>
                                                                        <p className="vocab-inner">{definition.example}</p>
                                                                        {/* {console.log("synonym: ", synonym)} */}
                                                                    </div>
                                                                    :
                                                                    ""}

                                                            </div>

                                                        )



                                                    }) :
                                                        ""}


                                                </CardText>
                                            </Card>
                                        </p>
                                    </div>
                                )

                            }) :

                                ""}

                            {/* Hi */}

                        </CardBody>
                    </div>
                )
            })}        
        </div>
    )

}