import { MyVocabAction } from './action';
import { MyVocabState, initialState} from './state';
import decodeJWT from 'jwt-decode';



export let MyVocabReducer = (
    state: MyVocabState = initialState,
    action: MyVocabAction,
): MyVocabState => {
    switch (action.type) {
        case '@@MyVocab/getAllMyVocabSuccess':
            try {
                // const user: AuthUser = decodeJWT(action.token)
                // return {
                //     ...state,
                //     vocabId: action.vocabId,
                //     word: action.word,
                //     phonetics: action.phonetics,
                //     meanings: action.meanings,
                //     error: null,
                //     user,
                // }
                return {
                    ...state,
                    words: Object.values(Object.fromEntries([...state.words||[], ...action.words].map(x => [x.id, x]))),
                }

            } catch (error) {
                return {
                    ...state,
                    error: 'get my vocab fail',
                    // token: null,
                    // vocabId: null,
                    words: null,
                    // phonetics: null,
                    // meanings: null,
                }
            }

        case '@@MyVocab/getAllMyVocabFail':
            return {
                ...state,
                error: action.msg,
            }

        case '@@MyVocab/deleteVocabSuccess':

            try {
                return {
                    ...state,
                    words: state.words?state.words.filter(word => word.id !== action.id):null
                }
            } catch (error) {
                return {
                    ...state,
                    error: 'fail to delete vocab',
                    vocabId: null,
                }
            }

        case '@@MyVocab/deleteVocabFail':
            return {
                ...state,
                error: action.msg,
            }

        default:
            return state;

    }


}





