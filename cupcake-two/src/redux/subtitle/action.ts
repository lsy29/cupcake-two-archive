export function loadSubtitle(videoId:number){
  return{
      type: "@@LOAD_SUBTITLE", 
      videoId
  }
}
export type SubtitleAction = ReturnType<typeof loadSubtitle>

