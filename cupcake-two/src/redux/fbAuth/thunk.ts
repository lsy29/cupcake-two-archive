import { failedAction, loadTokenAction, logoutAction } from "./action";
import { IRootThunkDispatch } from "../store";
import { history }from '../store'
import { push, replace } from 'connected-react-router'

const {REACT_APP_API_SERVER} = process.env

export function logoutThunk() {
    return (dispatch: IRootThunkDispatch) => {
        localStorage.removeItem('token');
        dispatch(logoutAction())
    }
}

export function loginFacebookThunk(accessToken:string){
    return async(dispatch:any)=>{
        const res = await fetch(`${REACT_APP_API_SERVER}/login/facebook`,{
            method:'POST',
            headers:{
                "Content-Type":"application/json; charset=utf-8"
            },
            body: JSON.stringify({ accessToken })
        })
        const result = await res.json();

        if(res.status!==200){
            dispatch(failedAction(result.msg));
        }else{
            let { token } = result.data
            localStorage.setItem('token',token);
            dispatch(loadTokenAction(token));
            dispatch(push("/"));
        }
    }
}