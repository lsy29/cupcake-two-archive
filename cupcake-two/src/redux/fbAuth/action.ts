export function logoutAction () {
    return {
        type: '@@Auth/logout' as const,        
    }

}

export function failedAction (msg: string) {
    return {
        type: '@@Auth/failed' as const,
        msg,
    }

}

export function loadTokenAction (token: string) {
    return {
        type: '@@Auth/load_token' as const,  
        token,
    }

}


export type FBAuthAction = 
 | ReturnType<typeof logoutAction>
 | ReturnType<typeof failedAction>
 | ReturnType<typeof loadTokenAction>