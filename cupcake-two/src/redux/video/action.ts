export function selectVideo(videoId:number){
    return{
        type: "@@SELECT_VIDEO", 
        videoId
    }
}
export function selectCat(catId:number){
    return{
        type: "@@SELECT_CAT", 
        catId
    }
}

export type VideosAction = ReturnType<
        | typeof selectVideo
        | typeof selectCat
        >

