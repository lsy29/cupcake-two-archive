import { Dispatch } from "react"
import { selectCat, selectVideo} from "./action"

// export interface Video {
//     video_genre:string
//     youtube_video_id: any,
//     video_title: string,
//     id: number
// }

export interface VideoState {
    selectedVideo:string,
    selectedCat:string
}

const initialState: VideoState = 
// || is for 包底
JSON.parse(localStorage.getItem('video')! ) || {
    selectedVideo:'',
    selectedCat:''
    }


export const videoReducer = (state: VideoState = initialState, action: any): VideoState => {
    if (action.type === '@@SELECT_VIDEO'){
        console.log("action_video_id", action.videoId)
        let updatedState = {...state, selectedVideo : action.videoId}
        localStorage.setItem('video',JSON.stringify(updatedState))
        return updatedState
    }
    if (action.type ==='@@SELECT_CAT'){
//testing testing testing testing testing testing testing testing testing testing testing testing testing testing 
        // return initialState
        console.log("action_cat_id", action.catId)
        let updatedState = {...state, selectedCat : action.catId}
        localStorage.setItem('cat',JSON.stringify(updatedState))
        return updatedState
    }
    return state
}



        // {
        //     ...state
        // }

        // {
        //     selectVideo:'2',
        //     selectCat:'2'
        // }