import {AuthAction} from './action'
import decodeJWT from 'jwt-decode'



export interface AuthState {
    token: string | null
    user: AuthUser | null
    error: string | null
}

export type AuthUser = {
    id: number,
    username: string,
    role:string,
    email: string,
    user_icon: string,
    
}

 // original code: 
 // const initialState: AuthState = {
//     token: localStorage.getItem('token'),
//     user: null,
//     error: null
// }
const initialState: AuthState = 
//f5 will delete user data if u comment the following sentence
JSON.parse(localStorage.getItem('user')! ) || 
  {token: localStorage.getItem('token'),
  user: null,
  error: null
}

export const authReducer = (state:AuthState = initialState, action: AuthAction): AuthState => {
    
    switch (action.type) {
        case '@@auth/LOGOUT_SUCCESS':
          localStorage.clear()
            return {
              token: null,
              user: null,
              error: null,
            }
 
            
        case '@@auth/LOGIN_FAILED':
          return {
            error: action.msg,
            user: null,
            token: null,
            
            
          }
        case '@@auth/LOGIN_SUCCESS':
          try {
            // original code: 
            // const user: AuthUser = decodeJWT(action.token)
            // localStorage.setItem('token',action.token)
            // return {
            //   error: null,
            //   user,
            //   token: action.token,
            // }

            const user: AuthUser = decodeJWT(action.token)
            let updatedState = {
              error: null,
              user,
              token: action.token,
            }
            localStorage.setItem('token',action.token)
            localStorage.setItem('user',JSON.stringify(updatedState))
            return updatedState

          } catch (error) {
            return {
              error: 'invalid Token',
              user: null,
              token: null,
            }
          }
    
        default:
    
    
    return state; 
  } 
}