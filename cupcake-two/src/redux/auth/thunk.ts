
import { push} from "connected-react-router"
import { RootState } from '../store'
import { IRootThunkDispatch } from '../store'
import { loginSuccess, logoutSuccess } from './action'
import Swal from 'sweetalert2'
import { history } from '../store';

//edgar

const {REACT_APP_API_SERVER} = process.env

export function logoutThunk() {
  return (dispatch: IRootThunkDispatch) => {

    localStorage.removeItem('token')

    dispatch(logoutSuccess())
    history.push('/')
  }
}





export function loginThunk(loginForm: any) {
  return async (dispatch: any) => {
    //edgar
    const res = await fetch(`${REACT_APP_API_SERVER}/login`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json; charsct=utf-8'

      },
      body: JSON.stringify(loginForm)
    })

    let result: any = await res.json()
    console.log(result.token)
    if (res.status === 200) {
      Swal.fire({
        position: 'center',
        icon: 'success',
        title: 'Login sucess',
        showConfirmButton: false,
        timer: 1000
      })

      dispatch(loginSuccess(result.token))
      dispatch(push('/'))
      console.log('to home page')

    }


  }
}



export function autoLoginThunk() {
  return async (dispatch: any, getState: () => RootState) => {

    let token = getState().auth.token

    if (token) {

      dispatch(loginSuccess(token))
    }


  }
}

