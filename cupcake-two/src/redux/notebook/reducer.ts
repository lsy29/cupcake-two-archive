import { log } from "console"
import { NotebookAction } from "./action"

export interface Word {
  word?: string
  id?: number
  phonetics  ?: []
  meanings?: []
}

export interface NotebookState {
  word: Word | null
  user_id: number | null
  word_id: number | null
  msg: string| null
  vocabs: string | null
}


const initialState: NotebookState = {
  word: null,
  user_id: null,
  word_id: null,
  msg: '',
  vocabs: null
}

export const notebookReducer = (state: NotebookState = initialState, action: NotebookAction): NotebookState => {
  console.log('action = ', action)

  switch (action.type) {
    case '@@notebook/LOAD_WORD':
      return {
        ...state,
        word: {...state.word, ...action.vocabs},
      }
    case '@@notebook/LOAD_WORD/failed':
      return {
        ...state,
        msg: action.msg
      }
    case '@@notebook/ADD_MY_VOCAB':
      return {
        ...state,
        vocabs: action.vocabs
        
        
      }

    case '@@notebook/DELETE_MY_VOCAB':
      return {
       ...state

      }

    default:
      return state;
  }
}
