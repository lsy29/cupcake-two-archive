// import { push } from "connected-react-router"
// import {history }from '../store'

import { loadAllMyVocabThunk } from "../myVocab/thunk"
import { loadNoteBook, loadNoteBookFail, addMyVocab } from "./action"

const {REACT_APP_API_SERVER} = process.env



export function searchVocabThunk(seachWord:string) {
    return async (dispatch: any) => {
        //edgar
        const res = await fetch(`${REACT_APP_API_SERVER}/search/${seachWord}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json; charsct=utf-8',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })

        let result: any = await res.json()
        console.log(result)

        if (res.ok) {
            dispatch(loadNoteBook(result))
        }else{
            dispatch(loadNoteBookFail('load notebook fail'))
        }


    }
}




export function addVocabThunk(user_id: number, vocabs: string | undefined) {
    return async (dispatch: any) => {
        //edgar
        const res = await fetch(`${REACT_APP_API_SERVER}/myVocab`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json; charsct=utf-8',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                myVocabWord: vocabs,
            }),
        })
console.log ("vocabs:" ,vocabs)
        let result: any = await res.json()
        console.log(result)

        if (res.ok) {
            dispatch(loadAllMyVocabThunk())
        }


    }
}