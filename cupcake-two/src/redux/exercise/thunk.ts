import { push, replace } from 'connected-react-router'
import {history, IRootThunkDispatch }from '../store'
//edgar
const {REACT_APP_API_SERVER} = process.env
export function exerciseThunk (exerciseForm:any) {
    return async (dispatch:any)=> {
        //edgar
        const res = await fetch (`${REACT_APP_API_SERVER}/answer`,{
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                },
                body: JSON.stringify(exerciseForm)
        })
        console.log("exerciseForm",exerciseForm)
        console.log("@@@ exerciseThunk-res @@@",res)
        if (res.status === 200){
            dispatch(push('/check-answer'))
            console.log('to check-answer-page')
        }
    } 
} 

// export function postAnswer(form : HTMLFormElement) {

//     let formData = new FormData(form)
//     console.log(formData)
  
//     return async (dispatch: IRootThunkDispatch) => {
//       try {
//         console.log("posting answer !!! ")
//         let res = await api.post('/answer', formData)
        
//         let imgname = await res.text()
//         let imgpath = ""+ REACT_APP_API_SERVER + "/" + imgname
//         // let imgpath =  imgname
//         console.log(imgpath)
//         dispatch(submitFormSuccess(res.answer))
//         // let memo: Memo = await res.json()
//         // form.reset()
//         // dispatch(loadedMemos([memo]))
  
//       } catch (error) {
//         Swal.showValidationMessage(error.message)
//       }
//     }
  
//   }