import { Contact } from '../layout/footer'
import { Navigation } from '../layout/navigation'
import {
  Container, Row, Jumbotron, Button,
  Card, CardText, CardBody, CardHeader,
} from 'reactstrap';
import { useDispatch, useSelector } from 'react-redux';
// import { push } from 'connected-react-router'
import { useEffect, useState } from 'react';
// import { getByTestId, getElementError } from '@testing-library/react';
// import { useParams, useRouteMatch } from 'react-router';
import { IRootState } from '../redux/store';
import './VideoSubtitlePage.css'
// import { yellow } from '@material-ui/core/colors';
import { searchVocabThunk } from '../redux/notebook/thunk';
import DictionarySearchSubtitle from '../components/DictionarySearchSubtitle';
// import { DictionaryIcon } from '../components/DictionaryIcon';
import Swal from 'sweetalert2';
// import { autoLoginThunk } from '../redux/auth/thunk';
import { history } from '../redux/store'




const { REACT_APP_API_SERVER } = process.env

export default function VideoTranscriptPage() {

  // const [subtitle, setSubtitle] = useState('');

  const dispatch = useDispatch();
  const [Subtitles, setSubtitle] = useState<any>(null)
  const [splitTextClicked, setSplitTextClicked] = useState(false);
  const [searchWord, setSearchWord] = useState('')
  const [searchBTNClicked, setSearchBTNClicked] = useState('false');
  const selectedVideoId = useSelector((state: IRootState) => state.video.selectedVideo)
  const utubeVideoId = `${Subtitles ? Subtitles.youtube_video_id : ''}`
  // const [isOpen, setIsOpen] = useState(true)


  // const words = useSelector(
  //   (state: IRootState) => state.notebook.word
  // )

  const token = useSelector(
    (state: IRootState) => state.auth.token
  )


  function getElement(subTitle: string) {
    let result = <div
      dangerouslySetInnerHTML={{
        __html: subTitle.replace(/([\w]+)/g, "<span>$1</span>")
      }}>
    </div>
    // document!.querySelector('#subtitleContent')!.addEventListener('mouseover', () => setOnClickEvent())

    setTimeout(() => {
      setOnClickEvent()
    }, 0);
    return result
  }
  useEffect(() => {
    fetch(`${REACT_APP_API_SERVER}/subtitle?vid=` + selectedVideoId)
      .then(response => response.json())
      .then(data => setSubtitle(data[0])); //data[0] coz it is an array
  }, []);


  useEffect(() => {

    dispatch(searchVocabThunk(searchWord))

  }, [splitTextClicked, searchWord])

  // <div><DictionaryIcon/></div>

  function gotoexercise() {
    if (!token) {
      Swal.fire({
        icon: "warning",
        position: 'center',
        title: 'Please login first',
        showConfirmButton: false,
        timer: 1500
      })
      history.push('/login')

    }
    else {
      history.push('/exercise')

    }

  }




  function searchWordByClick(searchWord: string) {

    console.log("DictionSearch component executed")

    setSearchWord(searchWord)

    // return searchWord;

  }

  function setOnClickEvent() {
    // console.log('initStatus = ', initStatus);

    // if (initStatus === 'init') {

    let parentElement: any = document.querySelector('#subtitleContent div')
    // console.log('parentElement=', parentElement);

    if (parentElement) {
      let children = parentElement.children;
      for (var i = 0; i < children.length; i++) {
        let spanChild = children[i];
        spanChild.onclick = function () {
          searchWordByClick(spanChild.innerText);
          setSplitTextClicked(true);
          setSearchBTNClicked('');
          // setIsOpen(false);
          // fetch(`${REACT_APP_API_SERVER}/search/${spanChild.innerText}`)
          // .then(response => response.json())
          // .then(searchResult => console.log(searchResult[0])); 
          console.log(spanChild.innerText);
        }
        spanChild.onmouseover = function hover() {
          this.classList.add('word-highlight')
        }
        spanChild.onmouseleave = function hover() {
          this.classList.remove('word-highlight')
        }
      }
    }
  }

  console.log('selectedVideoId = ', selectedVideoId);

  return (
    <div style={{ display: "flex", flexDirection: "column", minHeight: "100vh" }}>
      <Navigation />
      <div style={{ minHeight: "80vh" }}>
        <div style={{ fontSize: "50px", marginTop: "40px", marginBottom: "30px" }}>
          {/* related to line 14 */}
          {Subtitles ? Subtitles.video_title : ''}
        </div>
        <Row className="videoBoxContrainer">
          <iframe className="videoContent" id={selectedVideoId} src={`https://www.youtube.com/embed/${utubeVideoId}?enablejsapi=1&controls=1`}
            width="450" height="350"></iframe>
          {/* <div> */}
          {/* <Jumbotron className="Jumbotron-subtitle" style={{ width: "600px", height: "350px", marginLeft: "50px", overflowX: "scroll" }}> */}
          <Jumbotron className="Jumbotron-subtitle" style={{ marginLeft: '3rem', width: "auto", height: "50rem", overflowX: "scroll", gridArea: "subtitleBox" }}>
            {/* <Jumbotron className="Jumbotron-subtitle"> */}
            {/* <Jumbotron style={{ height: "350px", marginLeft: "50px", overflowX: "scroll" }}> */}

            <Container fluid>
              <p id="subtitleContent">
                {Subtitles ? getElement(Subtitles.subtitle) : ''}
              </p>

            </Container>

          </Jumbotron>
          {/* </div> */}
        </Row>
        <div style={{ display: "flex", justifyContent: "flex-end" }}>
          <Button style={{ margin: "30px", fontSize: "1.6rem" }} variant="info" onClick={() => gotoexercise()}>Proceed to Exercise</Button>{' '}
        </div>
        <DictionarySearchSubtitle searchBTNClicked={searchBTNClicked} setSearchBTNClicked={searchBTNClicked => setSearchBTNClicked(searchBTNClicked as any)} searchWord={searchWord} setSearchWord={searchWord => setSearchWord(searchWord as any)} />
      </div>
      <Contact />
    </div>
  )
}
