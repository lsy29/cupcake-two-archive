import Pvbox from '../components/img2word/previewbox'
import Dtbox from '../components/img2word/detectbox'
import Dtwds from '../components/img2word/detectwds'
import { Contact } from '../layout/footer'
import { Navigation } from '../layout/navigation'
import { useEffect, useState, FormEvent } from 'react';
import JsonData from '../data/data.json'
import {
  Container, Row, Col, Jumbotron, Card, CardImg, CardText, CardBody,
  CardTitle, CardSubtitle, Button, FormGroup, Label, CustomInput
} from 'reactstrap';
import { IRootState } from '../redux/store'; //get user
import { autoLoginThunk } from '../redux/auth/thunk';//get user
// Library insered by Albert -- START//
import { useForm } from 'react-hook-form'
import { useDispatch, useSelector } from 'react-redux';
import { sayHelloThunk, hiPyThunk, postImgThunk, dtImgObjThunk, imgDictThunk } from "../redux/img2word/thunk"
import tflogo from '../components/img2word/tflogo.svg';
import klogo from '../components/img2word/klogo.png';
import './img2wordTesting.css'
import DictionarySearchImag2Word from '../components/DictionarySearchImag2Word'
import DictionarySearchSubtitle from '../components/DictionarySearchSubtitle'
import DictionarySearch from '../components/DictionarySearch'
import { searchVocabThunk } from '../redux/notebook/thunk'
import { Pvboxtesting } from '../components/img2word/previewboxtesting'
// Library insered by Albert -- END//



export default function ImageDetection() {
  //   const dataItem = {
  //     url:'https://www.youtube.com/embed/iNb7FdG9ucw?controls=0',
  //     title:'Demo props'
  //   }

  const [imageResultClicked, setImageResultClicked] = useState(false)
  const dispatch = useDispatch();
  const info = useSelector((state: IRootState) => state.auth.user)
  const [clickToOpen, setClickToOpen] = useState('');
  const [searchWord, setSearchWord] = useState('')
  const [searchBTNClicked, setSearchBTNClicked] = useState<boolean>(false);
  const word = useSelector((state: IRootState) => state.img2word.i2wObject?.dtwords)

  const Loading = require('react-loading-animation');


  useEffect(() => {
    if (!info)
      dispatch(autoLoginThunk())
  }, [info])

  useEffect(() => {
    if (word && word.length > 0 && word[0] !=''){
      console.log("have word: ", word)
      setSearchBTNClicked(false);
    }

  }, [word])

  function sayHello() {
    dispatch(sayHelloThunk())
  }

  function hiPy() {
    dispatch(hiPyThunk())
  }

  function postImg(e: FormEvent) {
    e.preventDefault()
    const postImgForm = e.target as HTMLFormElement
    dispatch(postImgThunk(postImgForm))
  }

  const dtwarray = useSelector(
    (state: IRootState) => state.img2word.i2wObject?.dtwords
  )

  function imgDict(e: FormEvent) {

    e.preventDefault()
    const postWordForm = e.target as HTMLFormElement
    console.log('Current directory: ' + process.cwd());
    console.log(postWordForm)
    // dispatch(imgDictThunk(postWordForm))

  }

  function Wordlink() {
    function Resultedlinks(word: any) {
      word = word.slice(1, -1)
      return (
        <>
          <form
            id="post-imgpath"
            method="POST"
            action="/dtimgobj"
            encType="multipart/form-data"
            onSubmit={imgDict}
            onClick={() => resultWordClicked(word)}
          >

            <input id="img-detected-word" name="img-detected-word" type="hidden" value={word}></input>
            <input type="submit" className="submitlink" value={word} />

          </form>
        </>
      )
    }

    function resultWordClicked(word: string) {
      console.log("wordClicked:", word);
      dispatch(searchVocabThunk(word))
      setClickToOpen(word)
      // setSearchBTNClicked('true');
      // console.log("clickToOpen from detectwds:", word)
    }

    // return <>testing</>
    return word?.map(word => Resultedlinks(word))
  }



  return (
    <div>
      <Navigation />
      <div style={{ minHeight: "80vh" }}>
      {/* <div style={{ minHeight: "calc(100vh-90px-192px)" }}> */}

      <Container>

        <div style={{ fontSize: "50px", marginTop: "40px", display: "flex", justifyContent: "center" }}>
          Object-Text Detection
        </div>
        {/* <h1>Let's convert images to words!</h1> */}
        <div style={{ fontSize: "25px", display: "flex", justifyContent: "center" }}>
          <div>
            Hi <span style={{ color: "SteelBlue" }}> {info?.username} </span>! Let's post a picture and convert the objects into words.
          </div>
        </div>

      </Container>

      <Container >

        <div className="intro_img2word">

          <div>
            {/* <Row> */}
            <div>
              <form
                id="post-memo"
                method="POST"
                action="/reactgreet"
                encType="multipart/form-data"
                onSubmit={postImg}
              >

                <br></br>
                <br></br>

                <Row style={{
                  margin:"40px", display: "flex", flexDirection: "column", justifyContent: "space-around", alignContent: "space-around", alignItems: "center"

                }}>
                  <FormGroup>
                    <CustomInput type="file" id="exampleCustomFileBrowser" name="image" label="Choose picture" />
                  </FormGroup>
                  <Button style={{ fontSize: "1.3rem", fontWeight: "bold" }} type="submit" className="exercise-submit-button">Upload Picture</Button>
                </Row>

              </form>
            </div>

            {/* </Row> */}
            <br></br>
            <br></br>
            <Row>
            
              <Col md={6}>
                <Pvboxtesting setSearchBTNClicked={searchBTNClicked => setSearchBTNClicked(searchBTNClicked as any)}/></Col>

              <Col md={6}>
                <div style={{ backgroundColor: "8DABD6", fontWeight: "bold" }}>Words Detected</div> <br />
                {/* <div style={{fontWeight:"bold", color:"gray"}}>{Wordlink()}</div> */}
   
                {searchBTNClicked ? <Loading/> 
                :
                <div style={{fontWeight:"bold", color:"gray"}}>{Wordlink()}</div>
                }
              </Col>

            </Row>

          </div>
          <DictionarySearchImag2Word clickToOpen={clickToOpen} setClickToOpen={clickToOpen => setClickToOpen(clickToOpen as any)} />
        </div>
      </Container>
      </div>
      <Contact />
    </div>
  )
}