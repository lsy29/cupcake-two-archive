import Pvbox from '../components/img2word/previewbox'
import Dtbox from '../components/img2word/detectbox'
import Dtwds from '../components/img2word/detectwds'

import { Contact } from '../layout/footer'
import { Navigation } from '../layout/navigation'
import { useEffect, useState, FormEvent } from 'react';
import JsonData from '../data/data.json'
import {
  Container, Row, Col, Jumbotron, Card, CardImg, CardText, CardBody,
  CardTitle, CardSubtitle, Button
} from 'reactstrap';


import { IRootState } from '../redux/store'; //get user
import { autoLoginThunk } from '../redux/auth/thunk';//get user
// Library insered by Albert -- START//
import { useForm } from 'react-hook-form'
import { useDispatch, useSelector } from 'react-redux';

import { sayHelloThunk, hiPyThunk, postImgThunk, dtImgObjThunk, imgDictThunk } from "../redux/img2word/thunk"

import tflogo from '../components/img2word/tflogo.svg';
import klogo from '../components/img2word/klogo.png';

import './img2word.css'
import DictionarySearchImag2Word from '../components/DictionarySearchImag2Word'
import DictionarySearchSubtitle from '../components/DictionarySearchSubtitle'
import DictionarySearch from '../components/DictionarySearch'
import { searchVocabThunk } from '../redux/notebook/thunk'


// Library insered by Albert -- END//



export default function Img2wordPage() {
  //   const dataItem = {
  //     url:'https://www.youtube.com/embed/iNb7FdG9ucw?controls=0',
  //     title:'Demo props'
  //   }

  const [imageResultClicked, setImageResultClicked] = useState(false)


  const dispatch = useDispatch();
  const info = useSelector((state: IRootState) => state.auth.user)
  const [clickToOpen, setClickToOpen] = useState('');
  const [searchWord, setSearchWord] = useState('')
  const word = useSelector((state: IRootState) => state.img2word.i2wObject?.dtwords)


  useEffect(() => {
    if (!info)
      dispatch(autoLoginThunk())
  }, [info])

  // const dispatch = useDispatch();


  // event > first-time dispatch , start //
  function sayHello() {
    dispatch(sayHelloThunk())
  }

  function hiPy() {
    dispatch(hiPyThunk())
  }


  function postImg(e: FormEvent) {
    e.preventDefault()
    const postImgForm = e.target as HTMLFormElement
    // console.log(postImgForm)
    dispatch(postImgThunk(postImgForm))
  }

  const dtwarray = useSelector(
    (state: IRootState) => state.img2word.i2wObject?.dtwords
  )

  function imgDict(e: FormEvent) {

    e.preventDefault()
    const postWordForm = e.target as HTMLFormElement
    console.log('Current directory: ' + process.cwd());
    console.log(postWordForm)
    // dispatch(imgDictThunk(postWordForm))

  }

  function Wordlink() {
    const dtwarray = useSelector(
      (state: IRootState) => state.img2word.i2wObject?.dtwords
    )
    function Resultedlinks(word: any) {
      word = word.slice(1, -1)

      return (
        <>
          <form
            id="post-imgpath"
            method="POST"
            action="/dtimgobj"
            encType="multipart/form-data"
            onSubmit={imgDict}
            onClick={() => resultWordClicked(word)}
          >


            <input id="img-detected-word" name="img-detected-word" type="hidden" value={word}></input>
            <input type="submit" className="submitlink" value={word} />

          </form>


        </>
      )


    }

    function resultWordClicked(word: string) {
      console.log("wordClicked:", word);
      dispatch(searchVocabThunk(word))
      setClickToOpen(word)
      // console.log("clickToOpen from detectwds:", word)
    }



    return dtwarray?.map(word => Resultedlinks(word))


  }

  // function previewImg(){
  //   dispatch(dtImgObjThunk())
  // }


  // function dtImgObj(){
  //   dispatch(dtImgObjThunk())
  // }
  // event > first-time dispatch , end //

  return (
    <div >
      <Navigation />
      {/* <Container> */}
      <Container >

      {/* <Container style={{minHeight:"80vh"}}> */}
       {/* <Container style={{ height:"100vh-92px-212px"}}> */}


        {/* <div style={{fontSize:"30px", marginBottom:"50px"}}> */}
        <div className="intro_img2word">

          <div style={{ paddingBottom: "10px" }}>
            <Row>
              <img style={{ width: "60%" }} src={tflogo}></img>
              <div style={{ width: "5%" }}></div>
              <img style={{ width: "35%" }} src={klogo}></img>

            </Row>
          </div>

          <div>
            <Row>

              <div style={{ width: "60%", border: "1px solid green" }}>
                <br></br>
                <span style={{ color: "green" }}>Keras Library :</span>
                <br></br>
                <a href="https://keras.io/examples/vision/retinanet/">Object Detection with RetinaNet</a>
                <br></br><br></br>

                <span style={{ color: "green" }}>Object Detection Model :</span><br></br>
                            RetinaNet <br></br>

                {/* <button onClick={sayHello}>Say Hello to EXPRESS</button><br></br><br></br> */}
                {/* <button onClick={hiPy}>Say Hello to Python</button><br></br><br></br> */}

                <br></br>
                <span style={{ color: "green" }}>COCO-2017 Dataset :</span><br></br>
                            There are 118k images.<br></br>
                            500 images are used for training.
                            <br></br><br></br>
              </div>

              <div style={{ width: "5%", textAlign: "left" }}></div>

              <div style={{ width: "35%", textAlign: "left" }}>
                <br></br>

                      Hi <span style={{ color: "brown" }}>{info?.username}</span> ! <br></br><br></br>
                      Let's post an image and detect words .<br></br><br></br>
                        Image Notes :<br></br>
                <form
                  id="post-memo"
                  method="POST"
                  action="/reactgreet"
                  encType="multipart/form-data"
                  onSubmit={postImg}
                >
                  {/* <label>Image Title : </label> */}
                  {/* <br></br><br></br> */}
                  <textarea
                    name="image_title"
                    className="memo-textarea"
                    placeholder="Optional"
                  ></textarea>
                  <br></br>
                  <input name="image" type="file" />
                  <br></br>
                  <input type="submit" value="Post and Preview" />
                </form>
              </div>

            </Row>
            <br></br>
            <br></br>
            <Row>
              <div style={{ width: "60%", textAlign: "center" }}><Pvbox /></div>
              <div style={{ width: "5%", textAlign: "center" }}></div>
              {/* <div style={{width:"35%", textAlign:"center"}}><Dtwds clickToOpen={clickToOpen} setClickToOpen={clickToOpen => setClickToOpen(clickToOpen as any)}/></div> */}
              <>
                <div>
                  <div style={{ backgroundColor: "skyblue" }}>Detected Words</div> <br />
                  <span>{Wordlink()}</span>
                </div>
                <br></br>
                <br></br>




              </>
            </Row>

          </div>


          <DictionarySearchImag2Word clickToOpen={clickToOpen} setClickToOpen={clickToOpen => setClickToOpen(clickToOpen as any)}/>

        </div>

      </Container>

      <Contact />
    </div>
  )
}