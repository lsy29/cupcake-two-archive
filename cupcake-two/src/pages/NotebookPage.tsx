import { Container } from 'reactstrap'
import DictionaryNote from '../components/DictionaryNote'
import DictionarySearch from '../components/DictionarySearch'
export default function NotebookPage() {
    return (
        <div>
          {/* <Container style={{ height:"100vh-92px-212px"}}> */}
          <DictionaryNote/>
          <DictionarySearch/>
          {/* </Container> */}
        </div>
    )
}

